package com.amteklab.parkhere;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class LoginActivity extends AppCompatActivity {

    public static int SPLASH_TIME_OUT = 500;
    private CallbackManager callbackManager;
    protected AccessToken accessToken;
    protected String userId, userName, userEmail, userAvatar;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    private LoginButton loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_login);

        loginButton = (LoginButton) findViewById(R.id.lbFacebook);

        // Setting share session login
        pref = getApplicationContext().getSharedPreferences("SessionParkHere", 0);
        editor = pref.edit();

        // Kondisi Login atau tidak
        int isLoggedIn = pref.getInt("isLoggedIn", 0);
        if (isLoggedIn == 1) {
            loginButton.setVisibility(View.INVISIBLE);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                }
            }, SPLASH_TIME_OUT);
        } else {
            // Event Login Facebook
            loginButton.setReadPermissions(Arrays.asList("public_profile", "user_friends", "email"));
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    loginButton.setVisibility(View.INVISIBLE);
                    accessToken = loginResult.getAccessToken();
                    GraphRequest request = GraphRequest.newMeRequest(
                            accessToken,
                            new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(
                                        JSONObject object,
                                        GraphResponse response) {
                                    if (response.getError() != null) {
                                        System.out.println("GraphRequest ERROR");
                                    } else {
                                        System.out.println("GraphRequest SUCCESS");
                                        JSONObject jsonData = response.getJSONObject();

                                        getFacebookItems(jsonData);

                                        // Go to MainActivity with data user
                                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                        startActivity(intent);
                                        LoginActivity.this.finish();
                                    }
                                }
                            });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id,name,link,email,picture");
                    request.setParameters(parameters);
                    request.executeAsync();
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException error) {
                    error.printStackTrace();
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void getFacebookItems(JSONObject jsonData) {
        try {
            userId = jsonData.getString("id");
            userName = jsonData.getString("name");
            userEmail = jsonData.getString("email");
            userAvatar = "https://graph.facebook.com/" + userId + "/picture?type=large";

            // Storing share session login
            editor.putInt("isLoggedIn", 1);
            editor.putString("userId", userId);
            editor.putString("userName", userName);
            editor.putString("userEmail", userEmail);
            editor.putString("userAvatar", userAvatar);
            editor.commit(); // Commit storing

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void generateHashForFacebook() {
        try {
            PackageInfo info = getPackageManager().
                    getPackageInfo("com.amteklab.parkhere", PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash Facebook : ", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("Message FB : ", e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            Log.e("Message FB : ", e.getMessage());
        }
    }
}
